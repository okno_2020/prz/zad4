# Zadanie 4 – z tablicą dwuwymiarową (0.75 pkt)

1. Tablicę A\[w\]\[k\] (w, k - stałe) wypełnić liczbami losowymi całkowitymi dwucyfrowymi (dodatnimi lub ujemnymi).
1. Wydrukować tablicę wierszami z ustawioną szerokością wydruku, uzupełniając każdy wiersz na końcu średnią wartością wszystkich liczb w tym wierszu, wydrukowaną z dokładnością do 2 miejsc po kropce.
1. Przesunąć cyklicznie o 1 miejsce w prawo wiersze, w których ta średnia jest większa co do modułu od stałej G, po czym ponownie wydrukować tablicę.
1. Dla każdej kolumny wydrukować (pod tą kolumną), ile jest w niej liczb podzielnych przez stałą D.
1. Wydrukować indeksy kolumn, które mają najmniej liczb podzielnych przez stałą D.

> Wskazówka: wypełnianie tablicy można wykonać przez losowanie wartości dodatniej i losowanie mnożnika.